async function fetchData() {
    const url = 'https://omgvamp-hearthstone-v1.p.rapidapi.com/info';
    const options = {
        method: 'GET',
        headers: {
            'X-RapidAPI-Key': '077b72b3f0msh935924d6cd1c157p15e79fjsn15a74ca1f991',
            'X-RapidAPI-Host': 'omgvamp-hearthstone-v1.p.rapidapi.com'
        }
    };

    try {
        const response = await fetch(url, options);
        const result = await response.json();
        
        console.log(result);
        console.log(result.classes);
        document.getElementById("concerts").innerHTML = result.classes.map(item => `<li>${item}</li>`).join('');

    } catch (error) {
        console.error(error);
    }
};

fetchData();
